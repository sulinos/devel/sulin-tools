WORKDIR=$1
mkdir -p ${WORKDIR}/lib 2>/dev/null || true
mkdir -p ${WORKDIR}/bin 2>/dev/null || true
copy_binary(){
	for bins in $*
	do
		libs="$(ldd $(which $bins) | grep '=>' | awk '{print $1}')"
		for i in $libs
		do
			lib=$(basename $i)
			if [ ! -f "${WORKDIR}/$lib" ] ; then
				[ -f /lib/$lib ] && LIBDIR=/lib
				[ -f /lib64/$lib ] && LIBDIR=/lib64
				[ -f /usr/lib/$lib ] && LIBDIR=/usr/lib
				[ -f /usr/lib64/$lib ] && LIBDIR=/usr/lib64
				install ${LIBDIR}/$lib ${WORKDIR}/lib/$lib || true
			fi
		done
		install $(which $bins) ${WORKDIR}/bin/$bins
	done
}
install $(which busybox) ${WORKDIR}/bin/busybox
unshare -r chroot $1  /bin/busybox --install -s /bin
shift
copy_binary $@
